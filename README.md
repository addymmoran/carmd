# CarMD
Author: [Addy Moran](addymmoran@gmail.com)

## Purpose
![Man Pushing Car](https://cdn.pixabay.com/photo/2018/11/26/09/57/pushing-3839062_1280.png)
Car diagnostics can be frusterating! This tool is to designed to make understanding the problem easier. 

## Steps
1. Run ``npm start`` 
2. Go to ``http://localhost:5051``
3. Enter the error code you pulled from the car (if you need help, see [instructions by YourMechanic.com](https://www.yourmechanic.com/article/how-to-read-and-understand-check-engine-light-codes-by-jason-unrau))
4. Enter the make of your vehicle
5. Hit the ``Search`` button

![Demo](docs/CarMD_Demo.gif)

## Technical Approach
### Tools Used
* Front End: Node.JS, Pug, and Bootstrap
* Back End: Python3, SQLite

## Current Implementation
Currently, the database only contains error codes for Fords, Jeep, and Subarus. 