
CREATE TABLE Car (
	id INTEGER PRIMARY KEY AUTOINCREMENT, 
	make TEXT NOT NULL
);

CREATE TABLE Code (
	id INTEGER ASC PRIMARY KEY, 
	code TEXT NOT NULL,
	faultLocation TEXT,
	cause TEXT,
	manufacturer INTEGER, 
	FOREIGN KEY(manufacturer) REFERENCES Car(id)
); 