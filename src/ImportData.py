import sqlite3


def import_raw_data(filename):
	entries = []
	with open(filename, 'r') as fp:
		for line in fp.readlines():
			sections = line.strip().replace('	', ':').split(':')
			if len(sections) < 2: 
				pass # header files which look like: ['P1900 – P1999 (Transmission)\n'] or it doesn't ahve 
			else:
				code = sections[0].strip()
				location = sections[1].strip()
				cause = sections[2].strip() if len(sections) == 3 else "Unknown"					
				entries.append([code, location, cause])
	return entries


def add_to_db(conn, manufacturer, entries): 
	cursor = conn.cursor()
	# Get manufacturer ID
	cursor.execute("SELECT * FROM Car WHERE make=?", (manufacturer,))
	make_id = cursor.fetchone() # to get id
	if make_id:
		make_id = make_id[0]
	else: # didn't find one
		cursor.execute('''INSERT INTO Car (make) VALUES (?)''', (manufacturer, ))
		conn.commit()
		make_id = cursor.lastrowid

	for entry in entries:
		print(entry[0], entry[1], entry[2], make_id)
		cursor.execute('''INSERT INTO Code (code, faultLocation, cause, manufacturer) 
			VALUES (?, ?, ?, ?)''', (entry[0], entry[1], entry[2], make_id))
		conn.commit()



if __name__ == "__main__":
	data_sources = [
		 {"manufacturer": "Ford", "filename": "raw_data/FordTroubleCodes.txt"},
		 {"manufacturer": "Subaru", "filename": "raw_data/SubaruFaultCodes.txt"},
		 {"manufacturer": "Jeep", "filename": "raw_data/JeepTroubleCodes.txt"}
	]

	try:
		conn = sqlite3.connect('db/ErrorCodes.db')
	except Error as e:
		print(e)
		exit(-1)

	for data_source in data_sources:
		entries = import_raw_data(data_source.get("filename"))
		add_to_db(conn, data_source.get("manufacturer"), entries)

	conn.close()
