'use strict';

var express = require('express');
var router = express.Router();
var sqlite = require('sqlite'); 
const path = require('path');
const db_file = path.resolve(__dirname, '../src/db/ErrorCodes.db')
const { check, validationResult } = require('express-validator'); 

let Promise = require('bluebird');

router.get('/', function(req, res){
    res.render('index', {title: 'CarMD'});
});

router.post('/', [
		check('obdcode').isLength({ min: 5, max: 5}),
		check('make').isLength({min: 4, max: 6}),
	], async (req, res) => {
		let message = ""; 

		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			let error_obj = errors.array()[0]; 
			console.log(error_obj); 

			message = error_obj.msg + ": '" + error_obj.value + "' is not a valid value for the " +
					error_obj.param + " field"; 
			res.render('index', {title: 'CarMD', message: message });
			return; 
		}
	
		let obdcode = req.body.obdcode; 
		let make = req.body.make; 
		let results = []; 
		const dbPromise = sqlite.open(db_file, {Promise});

		try {
			const db = await dbPromise;
			let [result]  = await Promise.all([db.get('SELECT * FROM Car WHERE make=?', make)]); 
			console.log(result); 
			if (!result) {
				message = "Couldn't find any error codes for the make: " + make; 
				res.render('index', {title: 'CarMD', message: message });
				return;
			}
			let id = result.id; 
		 	[result] = await Promise.all([
				db.all('SELECT faultLocation, cause FROM Code WHERE code=? AND manufacturer=?', [obdcode, id])
			]); 
			for (let i = 0; i < result.length; i++) {
				results.push({
					"Symptom": result[i].faultLocation, 
					"CommonFix": result[i].cause
				});
			}
			if (results.length == 0) {
				message = "Couldn't find any results for error codes " + obdcode + " that are for " + make; 
				res.render('index', {title: 'CarMD', message: message });
				return;
			}
			res.render('index', {title: 'CarMD', message: message, results: results, obdcode: obdcode, make:make });
		} catch (err) {
			console.error(err); 
		}
});

module.exports = router;

