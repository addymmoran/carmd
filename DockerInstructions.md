# To build from src
1. ```docker build -t carmd .```
2. ```docker run -p 5051:5051 carmd```
3. Open a browser and go to ```localhost:5051```

# Misc
* Check what docker containers are running: ```docker ps```
* To access the "insides" of the contiainer: ```docker exec -t <name (can find from docker ps)> /bin/bash```
* To stop a container: ```docker stop <name (can find from docker ps)>```

